package com.tsc.skuschenko.tm.repository.model;

import com.tsc.skuschenko.tm.api.repository.model.IProjectRepository;
import com.tsc.skuschenko.tm.model.Project;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import javax.persistence.EntityManager;
import javax.persistence.TypedQuery;
import java.util.List;

public final class ProjectRepository
        extends AbstractRepository<Project>
        implements IProjectRepository {

    public ProjectRepository(@NotNull final EntityManager entityManager) {
        super(entityManager);
    }

    @Override
    public void clear(@NotNull String userId) {
        @NotNull final String query =
                "DELETE FROM ProjectDto e WHERE e.userId = :userId";
        entityManager.createQuery(query, Project.class)
                .setParameter("userId", userId)
                .executeUpdate();
    }

    @Override
    public void clearAllProjects() {
        @NotNull final String query = "DELETE FROM ProjectDto e";
        entityManager.createQuery(query, Project.class)
                .executeUpdate();
    }

    @Nullable
    @Override
    public List<Project> findAll() {
        @NotNull final String query = "SELECT e FROM ProjectDto e ";
        return entityManager.createQuery(query, Project.class)
                .getResultList();
    }

    @Nullable
    @Override
    public List<Project> findAllWithUserId(@NotNull String userId) {
        @NotNull final String query =
                "SELECT e FROM ProjectDto e WHERE e.userId = :userId";
        return entityManager.createQuery(query, Project.class)
                .setParameter("userId", userId)
                .getResultList();
    }

    @Nullable
    @Override
    public Project findById(@NotNull String id) {
        return entityManager.find(Project.class, id);
    }

    @Nullable
    @Override
    public Project findOneById(@NotNull String userId, @NotNull String id) {
        @NotNull final String query =
                "SELECT e FROM ProjectDto e WHERE e.userId = :userId AND " +
                        "e.id = :id";
        TypedQuery<Project> typedQuery =
                entityManager.createQuery(query, Project.class)
                        .setParameter("id", id)
                        .setParameter("userId", userId);
        return getEntity(typedQuery);
    }

    @Nullable
    @Override
    public Project findOneByIndex(
            @NotNull String userId, @NotNull Integer index
    ) {
        @NotNull final String query =
                "SELECT e FROM ProjectDto e WHERE e.userId = :userId";
        TypedQuery<Project> typedQuery =
                entityManager.createQuery(query, Project.class)
                        .setParameter("userId", userId).
                        setFirstResult(index);
        return getEntity(typedQuery);
    }

    @Nullable
    @Override
    public Project findOneByName(
            @NotNull String userId, @NotNull String name
    ) {
        @NotNull final String query =
                "SELECT e FROM ProjectDto e WHERE e.userId = :userId AND " +
                        "e.name = :name";
        TypedQuery<Project> typedQuery =
                entityManager.createQuery(query, Project.class)
                        .setParameter("name", name)
                        .setParameter("userId", userId);
        return getEntity(typedQuery);
    }

    @Override
    public Project getReference(@NotNull final String id) {
        return entityManager.getReference(Project.class, id);
    }

    @Override
    public void removeOneById(
            @NotNull String userId, @NotNull String projectId
    ) {
        @NotNull final String query =
                "DELETE FROM ProjectDto e WHERE e.userId = :userId AND " +
                        "e.projectId = :projectId";
        entityManager.createQuery(query, Project.class)
                .setParameter("userId", userId)
                .setParameter("projectId", projectId)
                .executeUpdate();
    }

}