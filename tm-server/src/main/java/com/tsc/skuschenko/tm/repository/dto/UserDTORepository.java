package com.tsc.skuschenko.tm.repository.dto;

import com.tsc.skuschenko.tm.api.repository.dto.IUserDTORepository;
import com.tsc.skuschenko.tm.dto.UserDTO;
import com.tsc.skuschenko.tm.exception.entity.user.UserNotFoundException;
import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import javax.persistence.EntityManager;
import javax.persistence.TypedQuery;
import java.util.List;
import java.util.Optional;

public final class UserDTORepository extends AbstractDTORepository<UserDTO>
        implements IUserDTORepository {

    public UserDTORepository(@NotNull final EntityManager entityManager) {
        super(entityManager);
    }

    @Override
    public void clear() {
        @NotNull final String query = "DELETE FROM UserDTO e";
        entityManager.createQuery(query, UserDTO.class).executeUpdate();
    }

    @Override
    public @Nullable List<UserDTO> findAll() {
        @NotNull final String query = "SELECT e FROM UserDTO e ";
        return entityManager.createQuery(query, UserDTO.class).getResultList();
    }

    @Nullable
    @Override
    @SneakyThrows
    public UserDTO findByEmail(@NotNull final String email) {
        @NotNull final String query =
                "SELECT e FROM UserDTO e WHERE e.email = :email";
        TypedQuery<UserDTO> typedQuery =
                entityManager.createQuery(query, UserDTO.class)
                        .setParameter("email", email);
        return getEntity(typedQuery);
    }

    @Override
    public @Nullable UserDTO findById(@NotNull String id) {
        return entityManager.find(UserDTO.class, id);
    }

    @Nullable
    @Override
    @SneakyThrows
    public UserDTO findByLogin(@NotNull final String login) {
        @NotNull final String query =
                "SELECT e FROM UserDTO e WHERE e.login = :login";
        TypedQuery<UserDTO> typedQuery =
                entityManager.createQuery(query, UserDTO.class)
                        .setParameter("login", login);
        return getEntity(typedQuery);
    }

    @NotNull
    @Override
    @SneakyThrows
    public UserDTO lockUserByLogin(@NotNull final String login) {
        @NotNull final UserDTO user = Optional.ofNullable(findByLogin(login))
                .orElseThrow(UserNotFoundException::new);
        user.setLocked(true);
        update(user);
        return user;
    }

    @Nullable
    @Override
    @SneakyThrows
    public UserDTO removeByLogin(@NotNull final String login) {
        @Nullable final UserDTO user = findByLogin(login);
        Optional.ofNullable(user).ifPresent(this::update);
        return user;
    }

    @Override
    public void removeOneById(@NotNull String id) {
        @NotNull final String query = "DELETE FROM UserDTO e WHERE e.id = :id";
        entityManager.createQuery(query, UserDTO.class)
                .setParameter("id", id)
                .executeUpdate();
    }

    @NotNull
    @Override
    @SneakyThrows
    public UserDTO setPassword(
            @NotNull final String userId, @NotNull final String password
    ) {
        @NotNull final UserDTO user = Optional.ofNullable(findById(userId))
                .orElseThrow(UserNotFoundException::new);
        user.setPasswordHash(password);
        update(user);
        return user;
    }

    @NotNull
    @Override
    @SneakyThrows
    public UserDTO unlockUserByLogin(@NotNull final String login) {
        @NotNull final UserDTO user = Optional.ofNullable(findByLogin(login))
                .orElseThrow(UserNotFoundException::new);
        user.setLocked(false);
        update(user);
        return user;
    }

}
