package com.tsc.skuschenko.tm.dto;

import com.tsc.skuschenko.tm.api.entity.IWBS;
import lombok.NoArgsConstructor;

import javax.persistence.Entity;
import javax.persistence.Table;

@Entity
@NoArgsConstructor
@Table(name = "tm_project")
public final class ProjectDTO extends AbstractBusinessEntityDTO implements IWBS {

}
