package com.tsc.skuschenko.tm.model;

import com.tsc.skuschenko.tm.enumerated.Role;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.Nullable;

import javax.persistence.Column;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import java.util.ArrayList;
import java.util.List;

@Getter
@Setter
@NoArgsConstructor
@Table(name = "tm_user")
public final class User extends AbstractEntity {

    @Column
    @Nullable
    private String email;

    @Column
    @Nullable
    private String firstName;

    @Column(name = "locked")
    private boolean isLocked = false;

    @Column
    @Nullable
    private String lastName;

    @Column
    @Nullable
    private String login;

    @Column
    @Nullable
    private String middleName;

    @Column
    @Nullable
    private String passwordHash;

    @OneToMany(mappedBy = "user")
    @Nullable
    private List<Project> projects = new ArrayList<>();

    @Column
    @Nullable
    private String role = Role.USER.getDisplayName();

    @OneToMany(mappedBy = "user")
    @Nullable
    private List<Session> sessions = new ArrayList<>();

    @OneToMany(mappedBy = "user")
    @Nullable
    private List<Task> tasks = new ArrayList<>();

}
