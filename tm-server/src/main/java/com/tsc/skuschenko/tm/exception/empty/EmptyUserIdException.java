package com.tsc.skuschenko.tm.exception.empty;

import com.tsc.skuschenko.tm.exception.AbstractException;

public final class EmptyUserIdException extends AbstractException {

    public EmptyUserIdException() {
        super("Error! User Id is empty...");
    }

}
