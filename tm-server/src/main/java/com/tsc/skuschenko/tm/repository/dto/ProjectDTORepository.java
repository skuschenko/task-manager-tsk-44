package com.tsc.skuschenko.tm.repository.dto;

import com.tsc.skuschenko.tm.api.repository.dto.IProjectDTORepository;
import com.tsc.skuschenko.tm.dto.ProjectDTO;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import javax.persistence.EntityManager;
import javax.persistence.TypedQuery;
import java.util.List;

public final class ProjectDTORepository
        extends AbstractDTORepository<ProjectDTO>
        implements IProjectDTORepository {

    public ProjectDTORepository(@NotNull final EntityManager entityManager) {
        super(entityManager);
    }


    @Override
    public void clear(@NotNull String userId) {
        @NotNull final String query =
                "DELETE FROM ProjectDto e WHERE e.userId = :userId";
        entityManager.createQuery(query, ProjectDTO.class)
                .setParameter("userId", userId)
                .executeUpdate();
    }

    @Override
    public void clearAllProjects() {
        @NotNull final String query =
                "DELETE FROM ProjectDto e";
        entityManager.createQuery(query, ProjectDTO.class)
                .executeUpdate();
    }

    @Nullable
    @Override
    public List<ProjectDTO> findAll() {
        @NotNull final String query =
                "SELECT e FROM ProjectDto e ";
        return entityManager.createQuery(query, ProjectDTO.class)
                .getResultList();
    }

    @Nullable
    @Override
    public List<ProjectDTO> findAllWithUserId(@NotNull String userId) {
        @NotNull final String query =
                "SELECT e FROM ProjectDto e WHERE e.userId = :userId";
        return entityManager.createQuery(query, ProjectDTO.class)
                .setParameter("userId", userId)
                .getResultList();
    }

    @Nullable
    @Override
    public ProjectDTO findById(@NotNull String id) {
        return entityManager.find(ProjectDTO.class, id);
    }

    @Nullable
    @Override
    public ProjectDTO findOneById(@NotNull String userId, @NotNull String id) {
        @NotNull final String query =
                "SELECT e FROM ProjectDto e WHERE e.userId = :userId AND " +
                        "e.id = :id";
        TypedQuery<ProjectDTO> typedQuery =
                entityManager.createQuery(query, ProjectDTO.class)
                        .setParameter("id", id)
                        .setParameter("userId", userId);
        return getEntity(typedQuery);
    }

    @Nullable
    @Override
    public ProjectDTO findOneByIndex(
            @NotNull String userId, @NotNull Integer index
    ) {
        @NotNull final String query =
                "SELECT e FROM ProjectDto e WHERE e.userId = :userId";
        TypedQuery<ProjectDTO> typedQuery =
                entityManager.createQuery(query, ProjectDTO.class)
                        .setParameter("userId", userId).
                        setFirstResult(index);
        return getEntity(typedQuery);
    }

    @Nullable
    @Override
    public ProjectDTO findOneByName(
            @NotNull String userId, @NotNull String name
    ) {
        @NotNull final String query =
                "SELECT e FROM ProjectDto e WHERE e.userId = :userId AND " +
                        "e.name = :name";
        TypedQuery<ProjectDTO> typedQuery =
                entityManager.createQuery(query, ProjectDTO.class)
                        .setParameter("name", name)
                        .setParameter("userId", userId);
        return getEntity(typedQuery);
    }

    @Override
    public void removeOneById(
            @NotNull String userId, @NotNull String projectId
    ) {
        @NotNull final String query =
                "DELETE FROM ProjectDto e WHERE e.userId = :userId AND " +
                        "e.projectId = :projectId";
        entityManager.createQuery(query, ProjectDTO.class)
                .setParameter("userId", userId)
                .setParameter("projectId", projectId)
                .executeUpdate();
    }

}