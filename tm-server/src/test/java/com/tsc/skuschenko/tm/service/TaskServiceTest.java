package com.tsc.skuschenko.tm.service;

import com.tsc.skuschenko.tm.api.service.IConnectionService;
import com.tsc.skuschenko.tm.api.service.IPropertyService;
import com.tsc.skuschenko.tm.api.service.dto.ITaskDTOService;
import com.tsc.skuschenko.tm.dto.TaskDTO;
import com.tsc.skuschenko.tm.enumerated.Status;
import com.tsc.skuschenko.tm.service.dto.TaskDTOService;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.Assert;
import org.junit.Test;

public class TaskServiceTest {

    @Test
    public void testChangeStatusById() {
        @NotNull final ITaskDTOService
                taskService = testService();
        @NotNull final TaskDTO task = taskService.add(
                "72729b26-01dd-4314-8d8c-40fb8577c6b5",
                "name1", "des1"
        );
        Assert.assertNotNull(task);
        taskService.changeStatusById(
                task.getUserId(), task.getId(), Status.COMPLETE
        );
        Assert.assertNotNull(task.getStatus());
        @Nullable final TaskDTO taskFind =
                taskService.findOneById(task.getUserId(), task.getId());
        Assert.assertNotNull(taskFind.getStatus());
        Assert.assertEquals(Status.COMPLETE.getDisplayName(), taskFind.getStatus());
    }

    @Test
    public void testChangeStatusByName() {
        @NotNull final ITaskDTOService
                taskService = testService();
        @NotNull final TaskDTO task = taskService.add(
                "72729b26-01dd-4314-8d8c-40fb8577c6b5",
                "name1", "des1"
        );
        taskService.changeStatusByName(
                task.getUserId(), task.getName(), Status.COMPLETE
        );
        Assert.assertNotNull(task.getStatus());
        @Nullable final TaskDTO taskFind =
                taskService.findOneByName(
                        task.getUserId(), task.getName()
                );
        Assert.assertNotNull(taskFind.getStatus());
        Assert.assertEquals(
                Status.COMPLETE.getDisplayName(), taskFind.getStatus()
        );
    }

    @Test
    public void testCompleteById() {
        @NotNull final ITaskDTOService
                taskService = testService();
        @NotNull final TaskDTO task = taskService.add(
                "72729b26-01dd-4314-8d8c-40fb8577c6b5",
                "name1", "des1"
        );
        taskService.completeById(task.getUserId(), task.getId());
        Assert.assertNotNull(task.getStatus());
        @Nullable final TaskDTO taskFind =
                taskService.findOneById(task.getUserId(), task.getId());
        Assert.assertNotNull(taskFind.getStatus());
        Assert.assertEquals(
                Status.COMPLETE.getDisplayName(), taskFind.getStatus()
        );
    }

    @Test
    public void testCompleteByIndex() {
        @NotNull final ITaskDTOService
                taskService = testService();
        @NotNull final TaskDTO task = taskService.add(
                "72729b26-01dd-4314-8d8c-40fb8577c6b5",
                "name1", "des1"
        );
        taskService.completeByIndex(task.getUserId(), 0);
        Assert.assertNotNull(task.getStatus());
        @Nullable final TaskDTO taskFind =
                taskService.findOneByIndex(task.getUserId(), 0);
        Assert.assertNotNull(taskFind.getStatus());
        Assert.assertEquals(
                Status.COMPLETE.getDisplayName(), taskFind.getStatus()
        );
    }

    @Test
    public void testCompleteByName() {
        @NotNull final ITaskDTOService
                taskService = testService();
        @NotNull final TaskDTO task = taskService.add(
                "72729b26-01dd-4314-8d8c-40fb8577c6b5",
                "name1", "des1"
        );
        Assert.assertNotNull(task.getName());
        taskService.completeById(task.getUserId(), task.getId());
        @Nullable final TaskDTO taskFind =
                taskService.findOneByName(
                        task.getUserId(), task.getName()
                );
        Assert.assertNotNull(taskFind.getStatus());
        Assert.assertEquals(
                Status.COMPLETE.getDisplayName(), taskFind.getStatus()
        );
    }

    @Test
    public void testCreate() {
        @NotNull final ITaskDTOService
                taskService = testService();
        taskService.add(
                "72729b26-01dd-4314-8d8c-40fb8577c6b5",
                "name1", "des1"
        );
    }

    @Test
    public void testFindOneById() {
        @NotNull final ITaskDTOService
                taskService = testService();
        @NotNull final TaskDTO task = taskService.add(
                "72729b26-01dd-4314-8d8c-40fb8577c6b5",
                "name1", "des1"
        );
        @Nullable final TaskDTO taskFind =
                taskService.findOneById(task.getUserId(), task.getId());
        Assert.assertNotNull(taskFind);
    }

    @Test
    public void testFindOneByIndex() {
        @NotNull final ITaskDTOService
                taskService = testService();
        @NotNull final TaskDTO task = taskService.add(
                "72729b26-01dd-4314-8d8c-40fb8577c6b5",
                "name1", "des1"
        );
        @Nullable final TaskDTO taskFind =
                taskService.findOneByIndex(task.getUserId(), 0);
        Assert.assertNotNull(taskFind);
    }

    @Test
    public void testFindOneByName() {
        @NotNull final ITaskDTOService
                taskService = testService();
        @NotNull final TaskDTO task = taskService.add(
                "72729b26-01dd-4314-8d8c-40fb8577c6b5",
                "name1", "des1"
        );
        @Nullable final TaskDTO taskFind =
                taskService.findOneByName(
                        task.getUserId(), task.getName()
                );
        Assert.assertNotNull(taskFind);
    }

    @Test
    public void testRemoveOneById() {
        @NotNull final ITaskDTOService
                taskService = testService();
        @NotNull final TaskDTO task = taskService.add(
                "72729b26-01dd-4314-8d8c-40fb8577c6b5",
                "name1", "des1"
        );
        taskService.removeOneById(task.getUserId(), task.getId());
        @Nullable final TaskDTO taskFind =
                taskService.findOneById(task.getUserId(), task.getId());
        Assert.assertNull(taskFind);
    }

    @Test
    public void testRemoveOneByIndex() {
        @NotNull final ITaskDTOService
                taskService = testService();
        @NotNull final TaskDTO task = taskService.add(
                "72729b26-01dd-4314-8d8c-40fb8577c6b5",
                "name1", "des1"
        );
        taskService.removeOneByIndex(task.getUserId(), 0);
        @Nullable final TaskDTO taskFind =
                taskService.findOneById(task.getUserId(), task.getId());
        Assert.assertNull(taskFind);
    }

    @Test
    public void testRemoveOneByName() {
        @NotNull final ITaskDTOService
                taskService = testService();
        @NotNull final TaskDTO task = taskService.add(
                "72729b26-01dd-4314-8d8c-40fb8577c6b5",
                "name1", "des1"
        );
        taskService.removeOneByName(
                task.getUserId(), task.getName()
        );
        @Nullable final TaskDTO taskFind =
                taskService.findOneById(task.getUserId(), task.getId());
        Assert.assertNull(taskFind);
    }

    @NotNull
    private ITaskDTOService testService() {
        @NotNull final IPropertyService propertyService =
                new PropertyService();
        @NotNull final IConnectionService connectionService =
                new ConnectionService(propertyService);
        Assert.assertNotNull(connectionService);
        @NotNull final ITaskDTOService taskService =
                new TaskDTOService(connectionService);
        Assert.assertNotNull(taskService);
        return taskService;
    }

    @Test
    public void testStartById() {
        @NotNull final ITaskDTOService
                taskService = testService();
        @NotNull final TaskDTO task = taskService.add(
                "72729b26-01dd-4314-8d8c-40fb8577c6b5",
                "name1", "des1"
        );
        taskService.startById(task.getUserId(), task.getId());
        Assert.assertNotNull(task.getStatus());
        @Nullable final TaskDTO taskFind =
                taskService.findOneById(task.getUserId(), task.getId());
        Assert.assertNotNull(taskFind.getStatus());
        Assert.assertEquals(
                Status.IN_PROGRESS.getDisplayName(), taskFind.getStatus()
        );
    }

    @Test
    public void testStartByIndex() {
        @NotNull final ITaskDTOService
                taskService = testService();
        @NotNull final TaskDTO task = taskService.add(
                "72729b26-01dd-4314-8d8c-40fb8577c6b5",
                "name1", "des1"
        );
        taskService.startByIndex(task.getUserId(), 0);
        Assert.assertNotNull(task.getStatus());
        @Nullable final TaskDTO taskFind =
                taskService.findOneByIndex(task.getUserId(), 0);
        Assert.assertNotNull(taskFind.getStatus());
        Assert.assertEquals(
                Status.IN_PROGRESS.getDisplayName(), taskFind.getStatus()
        );
    }

    @Test
    public void testStartByName() {
        @NotNull final ITaskDTOService
                taskService = testService();
        @NotNull final TaskDTO task = taskService.add(
                "72729b26-01dd-4314-8d8c-40fb8577c6b5",
                "name1", "des1"
        );
        taskService.startByName(task.getUserId(), "name1");
        Assert.assertNotNull(task.getStatus());
        @Nullable final TaskDTO taskFind =
                taskService.findOneByName(
                        task.getUserId(), task.getName()
                );
        Assert.assertNotNull(taskFind.getStatus());
        Assert.assertEquals(
                Status.IN_PROGRESS.getDisplayName(), taskFind.getStatus()
        );
    }

    @NotNull
    private TaskDTO testTaskModel() {
        @Nullable final TaskDTO task = new TaskDTO();
        task.setUserId("72729b26-01dd-4314-8d8c-40fb8577c6b5");
        task.setName("name1");
        task.setDescription("des1");
        Assert.assertNotNull(task);
        Assert.assertNotNull(task.getUserId());
        Assert.assertNotNull(task.getName());
        Assert.assertEquals("name1", task.getName());
        return task;
    }

    @Test
    public void testUpdateOneById() {
        @NotNull final ITaskDTOService
                taskService = testService();
        @NotNull final TaskDTO task = taskService.add(
                "72729b26-01dd-4314-8d8c-40fb8577c6b5",
                "name1", "des1"
        );
        task.setName("name2");
        task.setDescription("des2");
        Assert.assertNotNull(task.getName());
        Assert.assertNotNull(task.getDescription());
        @Nullable final TaskDTO taskFind =
                taskService.findOneById(task.getUserId(), task.getId());
        Assert.assertNotNull(task.getName());
        Assert.assertNotNull(task.getDescription());
        Assert.assertEquals("name2", taskFind.getName());
        Assert.assertEquals("des2", taskFind.getDescription());
    }

    @Test
    public void testUpdateOneByIndex() {
        @NotNull final ITaskDTOService
                taskService = testService();
        @NotNull final TaskDTO task = taskService.add(
                "72729b26-01dd-4314-8d8c-40fb8577c6b5",
                "name1", "des1"
        );
        task.setName("name2");
        task.setDescription("des2");
        Assert.assertNotNull(task.getName());
        Assert.assertNotNull(task.getDescription());
        @Nullable final TaskDTO taskFind =
                taskService.findOneByIndex(task.getUserId(), 0);
        Assert.assertNotNull(task.getName());
        Assert.assertNotNull(task.getDescription());
        Assert.assertEquals("name2", taskFind.getName());
        Assert.assertEquals("des2", taskFind.getDescription());
    }

}