package com.tsc.skuschenko.tm.repository;

import com.tsc.skuschenko.tm.api.repository.dto.ISessionDTORepository;
import com.tsc.skuschenko.tm.api.service.IConnectionService;
import com.tsc.skuschenko.tm.api.service.IPropertyService;
import com.tsc.skuschenko.tm.dto.SessionDTO;
import com.tsc.skuschenko.tm.repository.dto.SessionDTORepository;
import com.tsc.skuschenko.tm.service.ConnectionService;
import com.tsc.skuschenko.tm.service.PropertyService;
import com.tsc.skuschenko.tm.util.SignatureUtil;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.Assert;
import org.junit.Test;

import javax.persistence.EntityManager;

public class SessionRepositoryTest {

    @NotNull
    final IPropertyService propertyService =
            new PropertyService();
    @NotNull
    final IConnectionService connectionService =
            new ConnectionService(propertyService);

    @NotNull
    final EntityManager entityManager =
            connectionService.getEntityManager();

    @Test
    public void testClear() {
        @Nullable final SessionDTO session = testSessionModel();
        @NotNull final ISessionDTORepository
                sessionRepository = testRepository(session);
        entityManager.getTransaction().begin();
        sessionRepository.remove(session);
        entityManager.getTransaction().commit();
        @Nullable final SessionDTO sessionFind =
                sessionRepository.findSessionById(session);
        entityManager.close();
        Assert.assertNull(sessionFind);
    }

    @Test
    public void testCreate() {
        @NotNull final SessionDTO session = testSessionModel();
        testRepository(session);
    }

    @Test
    public void testFindOneById() {
        @Nullable final SessionDTO session = testSessionModel();
        @NotNull final ISessionDTORepository
                sessionRepository = testRepository(session);
        @Nullable final SessionDTO sessionFind =
                sessionRepository.findSessionById(session);
        entityManager.close();
        Assert.assertNotNull(sessionFind);
    }

    @Test
    public void testRemoveOneById() {
        @Nullable final SessionDTO session = testSessionModel();
        @NotNull final ISessionDTORepository
                sessionRepository = testRepository(session);
        entityManager.getTransaction().begin();
        sessionRepository.removeSessionById(session);
        entityManager.getTransaction().commit();
        @Nullable final SessionDTO sessionFind =
                sessionRepository.findSessionById(session);
        entityManager.close();
        Assert.assertNull(sessionFind);
    }

    @NotNull
    private ISessionDTORepository testRepository(
            @NotNull final SessionDTO session
    ) {
        @NotNull final ISessionDTORepository sessionRepository =
                new SessionDTORepository(entityManager);
        Assert.assertTrue(
                sessionRepository.findAll(session.getUserId()).isEmpty()
        );
        entityManager.getTransaction().begin();
        sessionRepository.add(session);
        entityManager.getTransaction().commit();
        @Nullable final SessionDTO sessionById =
                sessionRepository.findSessionById(session);
        entityManager.close();
        Assert.assertNotNull(sessionById);
        Assert.assertEquals(sessionById.getId(), session.getId());
        return sessionRepository;
    }

    @NotNull
    private SessionDTO testSessionModel() {
        @Nullable final SessionDTO session = new SessionDTO();
        session.setUserId("72729b26-01dd-4314-8d8c-40fb8577c6b5");
        session.setId("Id1");
        session.setTimestamp(System.currentTimeMillis());
        String signature = SignatureUtil.sign(session, "password", 454);
        session.setSignature(signature);
        Assert.assertNotNull(session);
        Assert.assertNotNull(session.getTimestamp());
        Assert.assertNotNull(session.getId());
        Assert.assertNotNull(session.getUserId());
        Assert.assertNotNull(session.getSignature());
        Assert.assertEquals("UId1", session.getUserId());
        Assert.assertEquals("Id1", session.getId());
        Assert.assertEquals(
                signature,
                session.getSignature()
        );
        return session;
    }

}
