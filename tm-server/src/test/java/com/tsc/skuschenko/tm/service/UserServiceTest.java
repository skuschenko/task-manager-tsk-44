package com.tsc.skuschenko.tm.service;

import com.tsc.skuschenko.tm.api.service.IConnectionService;
import com.tsc.skuschenko.tm.api.service.IPropertyService;
import com.tsc.skuschenko.tm.api.service.dto.IUserDTOService;
import com.tsc.skuschenko.tm.dto.UserDTO;
import com.tsc.skuschenko.tm.enumerated.Role;
import com.tsc.skuschenko.tm.service.dto.UserDTOService;
import com.tsc.skuschenko.tm.util.HashUtil;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.Assert;
import org.junit.Test;

public class UserServiceTest {

    @Test
    public void testCreate() {
        @NotNull final IUserDTOService userService = testService();
        @NotNull final UserDTO user = userService.create("user1", "user1");
        Assert.assertNotNull(user);
        Assert.assertEquals("user1", user.getLogin());
    }

    @Test
    public void testCreateWithEmail() {
        @NotNull final IUserDTOService userService = testService();
        @NotNull final UserDTO user =
                userService.create("user1", "user1", "user1@user1.ru");
        Assert.assertNotNull(user);
        Assert.assertEquals("user1", user.getLogin());
        Assert.assertEquals("user1@user1.ru", user.getEmail());
    }

    @Test
    public void testCreateWithRole() {
        @NotNull final IUserDTOService userService = testService();
        @NotNull final UserDTO user =
                userService.create("user1", "user1", Role.USER);
        Assert.assertNotNull(user);
        Assert.assertEquals("user1", user.getLogin());
        Assert.assertEquals(Role.USER.getDisplayName(), user.getRole());
    }

    @Test
    public void testFindByEmail() {
        @NotNull final IUserDTOService userService = testService();
        userService.create("user1", "user1", "user1@user1.ru");
        @Nullable final UserDTO userFind =
                userService.findByEmail("user1@user1.ru");
        Assert.assertNotNull(userFind);
        Assert.assertEquals("user1@user1.ru", userFind.getEmail());
    }

    @Test
    public void testFindByLogin() {
        @NotNull final IUserDTOService userService = testService();
        userService.create("user1", "user1");
        @Nullable final UserDTO userFind =
                userService.findByLogin("user1");
        Assert.assertNotNull(userFind);
        Assert.assertEquals("user1", userFind.getLogin());
    }

    @Test
    public void testLockUserByLogin() {
        @NotNull final IUserDTOService userService = testService();
        @Nullable final UserDTO user = userService.create("user1", "user1");
        user.setLocked(true);
        @Nullable final UserDTO userFind =
                userService.findByLogin("user1");
        Assert.assertNotNull(userFind);
        Assert.assertTrue(userFind.isLocked());
    }

    @Test
    public void testRemoveByLogin() {
        @NotNull final IUserDTOService userService = testService();
        @Nullable final UserDTO user = userService.create("user1", "user1");
        userService.removeByLogin("user1");
        @Nullable final UserDTO userFind =
                userService.findByLogin("user1");
        Assert.assertNull(userFind);
        ;
    }

    @NotNull
    private IUserDTOService testService() {
        @NotNull final IPropertyService propertyService =
                new PropertyService();
        @NotNull final IConnectionService connectionService =
                new ConnectionService(propertyService);
        Assert.assertNotNull(connectionService);
        Assert.assertNotNull(propertyService);
        @NotNull final IUserDTOService userService =
                new UserDTOService(connectionService, propertyService);
        Assert.assertNotNull(userService);
        return userService;
    }

    @Test
    public void testSetPassword() {
        @NotNull final IUserDTOService userService = testService();
        @Nullable final UserDTO user = userService.create("user1", "user1");
        user.setPasswordHash(HashUtil.salt("secret", 35484, "password"));
        Assert.assertEquals(
                HashUtil.salt("secret", 35484, "password"),
                user.getPasswordHash());
    }

    @Test
    public void testUnlockUserByLogin() {
        @NotNull final IUserDTOService userService = testService();
        @Nullable final UserDTO user = userService.create("user1", "user1");
        user.setLocked(false);
        @Nullable final UserDTO userFind =
                userService.findByLogin("user1");
        Assert.assertNotNull(userFind);
        Assert.assertFalse(userFind.isLocked());
    }

    @Test
    public void testUpdateUser() {
        @NotNull final IUserDTOService userService = testService();
        @Nullable final UserDTO user = userService.create("user1", "user1");
        user.setFirstName("FirstName");
        user.setMiddleName("MiddleName");
        user.setLastName("LastName");
        Assert.assertNotNull(user.getFirstName());
        Assert.assertNotNull(user.getLastName());
        Assert.assertNotNull(user.getMiddleName());
        @Nullable final UserDTO userFind =
                userService.findByLogin("user1");
        Assert.assertEquals("FirstName", userFind.getFirstName());
        Assert.assertEquals("MiddleName", userFind.getMiddleName());
        Assert.assertEquals("LastName", userFind.getLastName());
    }

}
