package com.tsc.skuschenko.tm.command.project;

import com.tsc.skuschenko.tm.endpoint.Project;
import com.tsc.skuschenko.tm.endpoint.Session;
import com.tsc.skuschenko.tm.exception.entity.project.ProjectNotFoundException;
import com.tsc.skuschenko.tm.exception.entity.session.AccessForbiddenException;
import com.tsc.skuschenko.tm.util.TerminalUtil;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.util.Optional;

public final class ProjectChangeStatusByIdCommand extends AbstractProjectCommand {

    @NotNull
    private static final String DESCRIPTION = "change project by id";

    @NotNull
    private static final String NAME = "project-change-status-by-id";

    @Nullable
    @Override
    public String arg() {
        return null;
    }

    @NotNull
    @Override
    public String description() {
        return DESCRIPTION;
    }

    @Override
    public void execute() {
        @Nullable final Session session = serviceLocator.getSession();
        Optional.ofNullable(session).orElseThrow(AccessForbiddenException::new);
        showOperationInfo(NAME);
        showParameterInfo("id");
        @NotNull final String valueId = TerminalUtil.nextLine();
        @Nullable Project project = serviceLocator.getProjectEndpoint()
                .findProjectById(session, valueId);
        Optional.ofNullable(project).orElseThrow(ProjectNotFoundException::new);
        project = serviceLocator.getProjectEndpoint().changeProjectStatusById(
                session, valueId, readProjectStatus()
        );
        Optional.ofNullable(project).orElseThrow(ProjectNotFoundException::new);
        showProject(project);
    }

    @NotNull
    @Override
    public String name() {
        return NAME;
    }

}
